#!/usr/bin/env python

import os
import sys
import logging
import argparse
import subprocess
import ConfigParser


DEFAULT_CONFIG_FILE = '~/.nwrapper.cfg'


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(prog='nwrapper',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Nova Environment Wrapper')
    parser.add_argument('--config', dest='config_file', action='store',
                        default=DEFAULT_CONFIG_FILE, help='Configuration file')
    parser.add_argument('--env', dest='environment', action='store',
                        required=True, help='Environment we\'ll be working on')
    parser.add_argument(nargs='*', dest='nova_args', action='store',
                        help='Arguments to pass along to nova')

    args = parser.parse_args()

    config_file = os.environ.get('NWRAPPER_CONFIG')
    if config_file is not None:
        logging.warn('Using configuration file specified by NWRAPPER_CONFIG: {}'.format(config_file))
    else:
        config_file = args.config_file

    config_file = os.path.expanduser(config_file)
    if not os.path.exists(config_file):
        logging.error('Configuration file does not exist ({})'.format(config_file))
        return -1

    config = ConfigParser.SafeConfigParser()
    config.optionxform = str
    config.read(config_file)

    try:
        common = config.section('common')
    except Exception:
        common = {}

    environments = {}
    for section in config.sections():
        if section == 'common':
            pass

        environments[section] = {}
        for k, v in config.items(section):
            environments[section][k] = v

    if args.environment not in environments:
        logging.error('Requested an environment that we do not have a configuration for: {}'.format(args.environment))
        return -1

    environment = environments[args.environment]
    envpairs = dict(common.items() + environment.items())
    envpairs['PATH'] = os.environ.get('PATH', '.')

    subprocess.call(['nova'] + args.nova_args, env=envpairs)

    return 0

if __name__ == '__main__':
    sys.exit(main())
