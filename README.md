# nwrapper

You have multiple OpenStack environments, right?

But using environment variables to manage your credentials sucks!

## Install it

    git clone git@bitbucket.org:idlepattern/nwrapper.git
    cd nwrapper
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt

## Configure it

    cp nwrapper.cfg.example ~/.nwrapper.cfg
    vi ~/.nwrapper.cfg

## Use it

    ./nwrapper --env public list
    ./nwrapper --env private list

## Questions?

Join the Development room in HipChat and ask a question, or drop an e-mail to <dev@idlepattern.com>.

![Idle Pattern](http://www.idlepattern.com/img/logo-and-icon-transparent.png =150x)
